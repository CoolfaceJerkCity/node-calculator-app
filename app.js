import {crearArchivo} from './helpers/multiplicar.js';
import {argv} from './config/yargs.js';
import 'colors';

/* Imprimiendo variables de entorno */
console.log(process.argv);
console.log(argv);

crearArchivo(argv.b, argv.l, argv.u)
    .then(filename => console.log(filename.rainbow, 'creado correctamente'))
    .catch(err => console.log(err));