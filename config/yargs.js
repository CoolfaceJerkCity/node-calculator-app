import yargs from 'yargs'
import { hideBin } from 'yargs/helpers'

/* Configurando Yarg */
export const argv = yargs(hideBin(process.argv))
                .options({
                    'b': {
                        alias: 'base',
                        type: 'number',
                        demandOption: true,
                        describe: 'Es la base de la tabla de multiplicar'
                    },
                    'l': {
                        alias: 'list',
                        type: 'boolean',
                        default: false,
                        describe: 'Muestra la tabla en consola'
                    },
                    'u': {
                        alias: 'until',
                        type: 'number',
                        default: 10,
                        describe: 'Es la cantidad maxima de multiplicaciones'
                    }
                })
                .check((argv, options)=>{
                    if(isNaN(argv.b)){
                        throw 'La base tiene que ser un número';
                    }
                    if(isNaN(argv.u)){
                        throw 'La longitud tiene que ser un número';
                    }
                    return true;
                })
                .argv;