#Notas:
Este es mi primer programa en Node.js
```
Options:
--help     Show help   
[boolean]

--version  Show version number 
[boolean]

-b, --base     Es la base de la tabla de multiplicar       [number] [required]

-l, --list     Muestra la tabla en consola          
[boolean] [default: false]

-u, --until    Es la cantidad maxima de multiplicaciones[number] [default: 10]
```