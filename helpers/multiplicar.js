import { writeFileSync } from 'node:fs';
import 'colors';

export const crearArchivo = async(base = 5, list = false, until = 10) =>{
    try {
        let salida = '';
        let consola = '';
        
        for (let i = 1; i <= until; i++) {
            salida += `${base} x ${i} = ${base * i} \n`; 
            consola += `${base} ${'x'.green} ${i} ${'='.red} ${base * i} \n`;   
        }

        if(list){
            console.log('================='.green);
            console.log(`Tabla del: ${base}`);
            console.log('================='.green);
            console.log(consola);
        }

        writeFileSync(`./salida/tabla-${base}.txt`, salida);
        return `tabla-${base}.txt`;
        
    } catch (error) {
        throw error;
    }
};